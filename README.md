# Call for papers (CFP)

## Current functionality

* Webform template for call for papers
* Track chair user role

## Dependencies

* Webform

## Roadmap

* Conversion of form submission to session nodes
* Track chair voting for proposals (public or locked)
